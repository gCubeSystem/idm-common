# Identity Manager - common library

Common library for IDM service and client.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

[Identity Manager Service](https://wiki.gcube-system.org/gcube/SmartGears)

configuration: inserte

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Alfredo Oliviero**  [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## How to Cite this Software

Tell people how to cite this software. 
* Cite an associated paper?
* Use a specific BibTeX entry for the software?

    @software{gcat,
		author		= {{Alfredo Oliviero}},
		title		= {Identity Manager Service},
		abstract	= {This is an Identity Manager smargears service},
		url			=  {doi Zenodo URL}
		keywords	= {D4Science, gCube}
	}

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

