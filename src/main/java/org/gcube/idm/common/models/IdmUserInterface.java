package org.gcube.idm.common.models;

import java.util.List;
import java.util.Map;

public interface IdmUserInterface {
    public String getSelf();

    public String getId();

    public Long getCreatedTimestamp();

    public String getUsername();

    public Boolean isEnabled();

    // public Boolean isTotp();

    public Boolean isEmailVerified();

    public String getFirstName();

    public String getLastName();

    public String getEmail();

    public String getFederationLink();

    public String getServiceAccountClientId();

    public Map<String, List<String>> getAttributes();

    // public List<CredentialRepresentation> getCredentials();

    public List<String> getRequiredActions();

    // public List<FederatedIdentityRepresentation> getFederatedIdentities();

    // public List<SocialLinkRepresentation> getSocialLinks();

    public List<String> getRealmRoles();

    public Map<String, List<String>> getClientRoles();

    // public List<UserConsentRepresentation> getClientConsents();

    public Integer getNotBefore();

    public List<String> getGroups();

    public Map<String, Boolean> getAccess();
}
