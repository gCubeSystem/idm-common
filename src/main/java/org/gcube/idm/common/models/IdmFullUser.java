package org.gcube.idm.common.models;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// 
public class IdmFullUser extends IdmUser {

    protected String origin;

    // @JsonDeserialize(using = StringListMapDeserializer.class)
    protected Map<String, List<String>> attributes;
    // protected List<CredentialRepresentation> credentials;
    // protected Set<String> disableableCredentialTypes;
    protected List<String> requiredActions;
    // protected List<FederatedIdentityRepresentation> federatedIdentities;
    protected List<String> realmRoles;
    protected Map<String, List<String>> clientRoles;
    // protected List<UserConsentRepresentation> clientConsents;
    protected Integer notBefore;
    protected List<String> groups;
    protected String serviceAccountClientId; // For rep, it points to clientId (not DB ID)

    public Map<String, List<String>> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, List<String>> attributes) {
        this.attributes = attributes;
    }

    public List<String> getRequiredActions() {
        return requiredActions;
    }

    public void setRequiredActions(List<String> requiredActions) {
        this.requiredActions = requiredActions;
    }

    public List<String> getRealmRoles() {
        return realmRoles;
    }

    public void setRealmRoles(List<String> realmRoles) {
        this.realmRoles = realmRoles;
    }

    public Map<String, List<String>> getClientRoles() {
        return clientRoles;
    }

    public void setClientRoles(Map<String, List<String>> clientRoles) {
        this.clientRoles = clientRoles;
    }

    public Integer getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(Integer notBefore) {
        this.notBefore = notBefore;
    }

    public String getServiceAccountClientId() {
        return serviceAccountClientId;
    }

    public void setServiceAccountClientId(String serviceAccountClientId) {
        this.serviceAccountClientId = serviceAccountClientId;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public Map<String, List<String>> toAttributes() {
        Map<String, List<String>> attrs = new HashMap<>();

        if (getAttributes() != null)
            attrs.putAll(getAttributes());

        if (getUsername() != null)
            attrs.put("username", Collections.singletonList(getUsername()));
        else
            attrs.remove("username");

        if (getEmail() != null)
            attrs.put("email", Collections.singletonList(getEmail()));
        else
            attrs.remove("email");

        if (getLastName() != null)
            attrs.put("lastName", Collections.singletonList(getLastName()));

        if (getFirstName() != null)
            attrs.put("firstName", Collections.singletonList(getFirstName()));

        return attrs;
    }
}
