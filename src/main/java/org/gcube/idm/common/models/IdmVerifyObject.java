package org.gcube.idm.common.models;

import java.util.Map;

public class IdmVerifyObject {
    protected String jwt_token;
    protected String token;
    protected Map<String, Object> decoded;
    protected Map<String, Object> header;
    protected Map<String, Object> payload;
    protected Object jwt_decoded;

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Map<String, Object> getDecoded() {
        return decoded;
    }

    public void setDecoded(Map<String, Object> decoded) {
        this.decoded = decoded;
    }

    public Map<String, Object> getHeader() {
        return header;
    }

    public void setHeader(Map<String, Object> header) {
        this.header = header;
    }

    public Map<String, Object> getPayload() {
        return payload;
    }

    public void setPayload(Map<String, Object> payload) {
        this.payload = payload;
    }

    public Object getJwt_decoded() {
        return jwt_decoded;
    }

    public void setJwt_decoded(Object jwt_decoded) {
        this.jwt_decoded = jwt_decoded;
    }
}