package org.gcube.idm.common.is;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.security.AuthorizedTasks;
import org.gcube.common.security.secrets.Secret;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.InternalServerErrorException;
import jakarta.ws.rs.NotFoundException;

/**
 * Utility class to query EndPoints and search for AccessPoints from IS
 * 
 * @author Alfredo Oliviero (ISTI - CNR)
 */

public class InfrastrctureServiceClient {
    private static final Logger logger = LoggerFactory.getLogger(InfrastrctureServiceClient.class);

    // /**
    //  * obatins from IS the list of ServiceEndpoint matching the parameters
    //  *
    //  * @param resource_name
    //  * @param category
    //  * @param accessPointName
    //  * @param is_root_service
    //  * 
    //  * @return the list of EndPoints matching the parameters
    //  * @throws Exception
    //  */

    // public static List<ServiceEndpoint> getEndopintsFromIS(String resource_name, String category,
    //         boolean root_service) {

    //     Secret secret = getSecretForInfrastructure();
    //     return getEndopintsFromIS(resource_name, category, root_service, secret);
    // }



    /**
     * obatins from IS the list of ServiceEndpoint matching the parameters
     *
     * @param resource_name
     * @param category
     * @param root_service
     * @param secret
     * 
     * @return the list of EndPoints matching the parameters
     * @throws Exception
     */

    public static List<ServiceEndpoint> getEndopintsFromIS(String resource_name, String category,
            boolean root_service, Secret secret) {
        SimpleQuery query = queryFor(ServiceEndpoint.class);

        if (resource_name != null) {
            query.addCondition("$resource/Profile/Name/text() eq '" + resource_name + "'");
        }
        if (category != null) {
            query.addCondition("$resource/Profile/Category/text() eq '" + category + "'");
        }
        DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);

        List<ServiceEndpoint> endpoints = null;

        try {
            if (root_service) {

                endpoints = AuthorizedTasks.executeSafely(() -> {
                    // esegui la query
                    List<ServiceEndpoint> toReturn = client.submit(query);
                    return toReturn;
                }, secret);
            } else {
                endpoints = client.submit(query);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            throw new InternalServerErrorException(e);
        }

        return endpoints;

    }

    /**
     * obatains the list of AccessPoints matching the parameters
     *
     * @param resource_name
     * @param category
     * @param endPointName
     * @param is_root_service
     * @return the list of AccessPoints
     * @throws Exception
     */
    public static List<ServiceEndpoint.AccessPoint> getAccessPointsFromIS(String resource_name, String category,
            String endPointName, boolean is_root_service, Secret secret) {

        List<ServiceEndpoint> resources = getEndopintsFromIS(resource_name, category, is_root_service, secret);

        if (resources.size() == 0) {
            logger.error("There is no Runtime Resource having name " + resource_name + " and Category "
                    + category + " in this scope.");
            return null;
        }

        List<ServiceEndpoint.AccessPoint> response = new ArrayList<ServiceEndpoint.AccessPoint>();
        resources.forEach(res -> {
            Stream<ServiceEndpoint.AccessPoint> access_points_res = res.profile().accessPoints().stream();

            if (endPointName == null) {
                access_points_res = access_points_res.filter(ap -> ap.name().equals(endPointName));
            }

            access_points_res.forEach(a -> response.add(a));
        });
        return response;
    }

    /**
     * obatains the list of AccessPoints matching the parameters, and returns the
     * first one
     * 
     * @param resource_name
     * @param category
     * @param entryPointName
     * @return an AccessPoints matching the parameters
     * @throws Exception
     */
    public static ServiceEndpoint.AccessPoint getFirstAccessPointFromIS(String resource_name, String category,
            String entryPointName, boolean root_service, Secret secret) {

        List<ServiceEndpoint.AccessPoint> access_points = getAccessPointsFromIS(resource_name, category, entryPointName,
                root_service, secret);

        if (access_points == null || access_points.size() == 0) {
            logger.error("Unable to retrieve service endpoint " + entryPointName);
            return null;
        }

        return access_points.get(0);

    }

    /**
     * Reads the service configuration from the IS
     * 
     * @param resourceName
     * @param category
     * @param endPointName
     * @param is_root_service
     * @param secret
     * @return
     * @throws Exception
     */
    public static IsServerConfig serviceConfigFromIS(String resourceName, String category, String endPointName,
            boolean is_root_service, Secret secret)
            throws NotFoundException, ServerException {

        logger.info("Starting creating service credentials");
        ServiceEndpoint.AccessPoint accessPoint = InfrastrctureServiceClient.getFirstAccessPointFromIS(resourceName,
                category, endPointName, is_root_service, secret);
        if (accessPoint == null) {
            String error_log = "Unable to retrieve service endpoint " + endPointName;

            logger.error(error_log);
            throw new NotFoundException(error_log);
        }

        try {
            IsServerConfig config = new IsServerConfig(accessPoint);
            return config;

        } catch (Exception e) {
            logger.error("cannot create server config from {}", accessPoint);

            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }
}
